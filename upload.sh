#!/bin/bash
set -eux

ARCH=$1
TRIPLET=$2
TOOLCHAIN=apertis-$TRIPLET-toolchain

cd $(dirname $0)

upload_host="archive@images.apertis.org"
upload_root="/srv/images/public/daily/v2019"
upload_dest="$upload_host:$upload_root"
target="toolchain"
keyfile="/tmp/key"

apt -y update
apt -y install rsync ssh
# The environment variable does not contain the finishing `=` character
# Because gitlab only allows base64. Hence we add it here until we have
# file variables in gitlab 11.11 or 12.x
mkdir -p $(dirname "$keyfile")
touch "$keyfile" && chmod 0600 "$keyfile"
echo "$ARCHIVE_SECRET=" | base64 -d > "$keyfile"
BUILD_ID=$(date +%Y%m%d).$CI_PIPELINE_IID
ssh -oStrictHostKeyChecking=no -i "$keyfile" "$upload_host" mkdir -p "$upload_root/$BUILD_ID/$target/"
rsync -e "ssh -oStrictHostKeyChecking=no -i $keyfile" -aP $TOOLCHAIN.tar.xz "$upload_dest/$BUILD_ID/$target/"

