#!/bin/sh

set -e

[ $# = 2 ] || (echo "usage: $0 <arch> <triplet>" && exit 1)

if [ ! -d glibc_version_header ] ; then
	git clone https://github.com/wheybags/glibc_version_header.git
else
	echo "Glibc version header already exists"
fi
export VERSION_HEADER="$(pwd)/glibc_version_header/version_headers/x64/force_link_glibc_2.23.h"

ARCH=$1
TRIPLET=$2

SCRIPTS=$(dirname $0)
$SCRIPTS/build-gcc.sh      $ARCH $TRIPLET
$SCRIPTS/build-binutils.sh $ARCH $TRIPLET
$SCRIPTS/build-gdb.sh      $ARCH $TRIPLET
$SCRIPTS/build-libc.sh     $ARCH $TRIPLET


TOOLCHAIN=apertis-$TRIPLET-toolchain

# Debug symbols
rm -f *dbgsym*.deb
# ADA language
rm -f *gnat*.deb
# Fortran language
rm -f *fortran*.deb
# D programming languages
rm -f gdc-8*.deb
rm -f libgphobos*.deb
# Go language
rm -f *gccgo*.deb
rm -f libgo*.deb
# Objective C
rm -f libobjc*.deb

for var in *.deb; do
	dpkg -x $var $TOOLCHAIN
done

tar cf $TOOLCHAIN.tar $TOOLCHAIN
xz -z -e -T0 $TOOLCHAIN.tar
