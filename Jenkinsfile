#!/usr/bin/env groovy

env.PIPELINE_VERSION = VersionNumber(versionNumberString: '${BUILD_DATE_FORMATTED,"yyyyMMdd"}.${BUILDS_TODAY_Z}')

osname = 'apertis'

release = "v2019"

/* Determine whether to run uploads based on the prefix of the job name; in
 * case of apertis we expect the official jobs under apertis-<release>/ while
 * non-official onces can be in e.g. playground/ */
def production = env.JOB_NAME.startsWith("${osname}-")

docker_registry_name = 'docker-registry.apertis.org'
docker_image_name = "${docker_registry_name}/apertis/apertis-${release}-package-builder"

upload_host = "archive@images.apertis.org"
upload_root = "/srv/images/" + (production ? "public" : "test/${env.JOB_NAME}")
upload_dest = "${upload_host}:${upload_root}"
upload_credentials = '5a23cd79-e26d-41bf-9f91-d756c131b811'

test_repo_url = "git@gitlab.apertis.org:infrastructure/apertis-tests.git"
test_repo_credentials = 'df7b609b-df30-431d-a942-af263af80571'
test_repo_branch = 'apertis/' + release
test_lava_credentials = 'apertis-lava-user'

// SSH complains loudly if the current user is not in the user database, so fake that
def withSshAgent(credentials, command) {
  sshagent(credentials: [ credentials, ] ) {
    env.NSS_WRAPPER_PASSWD = "/tmp/passwd"
    env.NSS_WRAPPER_GROUP = '/dev/null'
    sh(script: 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}')
    sh(script: "export LD_PRELOAD=libnss_wrapper.so; ${command}")
  }
}

def uploadDirectory(source, target) {
  withSshAgent(upload_credentials, "ssh -oStrictHostKeyChecking=no ${upload_host} mkdir -p ${upload_root}/${target}/")
  withSshAgent(upload_credentials, "rsync -e 'ssh -oStrictHostKeyChecking=no' -aP ${source} ${upload_dest}/${target}/")
}

/** Generate the image name
 *
 * To have a single place for image name generation.
 *
 * @return string with the name
 */
def imageName(architecture, type, board, ostree = false){
  def name = osname
  if (ostree) {
    name = "${osname}_ostree"
  }
  return "${name}_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION}"
}

def submitTests(architecture, type, board, ostree = false) {
  def image_name = imageName(architecture, type, board, ostree)
  def version = env.PIPELINE_VERSION

  def name = osname
  if(ostree){
    name = "${osname}_ostree"
  }
  def profile_name = "${name}-${type}-${architecture}-${board}"

  dir ("apertis-tests") {
    git(url: test_repo_url,
        poll: false,
        credentialsId: test_repo_credentials,
        branch: test_repo_branch)
  }

  withCredentials([ file(credentialsId: test_lava_credentials, variable: 'lqaconfig'),
                    string(credentialsId: 'lava-phab-bridge-token', variable: 'token')]) {
    sh(script: """
    mkdir -p ${env.PIPELINE_VERSION}/meta/
    /usr/bin/lava-submit -c ${lqaconfig} \
      -g apertis-tests/templates/profiles.yaml \
      --profile ${profile_name} \
      --callback-secret ${token} \
      --callback-url https://lavaphabbridge.apertis.org/ \
      -t release:${release} \
      -t image_date:${version} \
      -t image_name:${image_name} 2>&1 | \
      tee ${env.PIPELINE_VERSION}/meta/lava-jobs-${name}-${architecture}-${type}-${board}
    """)
  }
}

def buildImage(architecture, type, triplet, production) {
  return {
    node("docker-slave") {
      checkout scm
        docker.withRegistry("https://${docker_registry_name}") {
          buildenv = docker.image(docker_image_name)
          /* Pull explicitely to ensure we have the latest */
          buildenv.pull()

          buildenv.inside("--device=/dev/kvm") {
            stage("setup ${architecture} ${type}") {
              env.PIPELINE_VERSION = VersionNumber(versionNumberString: '${BUILD_DATE_FORMATTED,"yyyyMMdd"}.${BUILDS_TODAY_Z}')
              sh ("env ; mkdir -p ${PIPELINE_VERSION}/${architecture}/${type}")
            }

          try {
            stage("${architecture} toolchain build") {
              sh(script: """
                cd ${PIPELINE_VERSION}/${architecture}/${type}
                ${WORKSPACE}/build-all.sh ${architecture} ${triplet}
                """)
            }
            stage("${architecture} ${type} apertis toolchain upload") {
              uploadDirectory (env.PIPELINE_VERSION, "daily/${release}")
            }

            // This stage must be the last in pipeline
            // a failure to submit the tests would break the builds
            stage("Tests for ${architecture} ${type} ${board}") {
              if (production) {
                submitTests(architecture, type, board, false)
                uploadDirectory (env.PIPELINE_VERSION, "daily/${release}")
              } else {
                println "Skipping submitting tests jobs for ${architecture} ${type}"
              }
            }
          } finally {
            stage("Cleanup ${architecture} ${type}") {
              deleteDir()
            }
          }
        }
      }
    }
  }
}


def images = [:]

// Types for all boards
def  types = [ [ "toolchain" ] ]

images += types.collectEntries { [ "Arm64 ${it[0]}": buildImage("arm64",
                                   it[0],
                                   "aarch64-linux-gnu",
                                   production ) ] }

images += types.collectEntries { [ "Armhf ${it[0]}": buildImage("armhf",
                                   it[0],
                                   "arm-linux-gnueabihf",
                                   production ) ] }

parallel images
