#!/bin/bash
set -ex

sudo docker build -t toolchain-builder .

WORK=$(pwd)/work
sudo rm -rf $WORK/*
mkdir -p $WORK
cp build-all.sh build-gcc.sh build-gdb.sh build-binutils.sh build-libc.sh $WORK
CNAME=toolchain-builder-container
docker rm $CNAME || true

RUNINBUILDER="docker run --rm --name $CNAME -v $WORK:/work -w /work"

cat <<EOF |
armhf arm-linux-gnueabihf
arm64 aarch64-linux-gnu
EOF
while read ARCH TRIPLET; do
	echo "$ARCH $TRIPLET"
	if ! $RUNINBUILDER toolchain-builder ./build-all.sh $ARCH $TRIPLET ; then
		echo "***** FAILED *****"
		echo "Build $ARCH $TRIPLET failed"
		echo "Inspect with: $RUNINBUILDER -ti toolchain-builder bash"
		echo "***** FAILED *****"
		exit 1
	fi
done

echo "Inspect with: $RUNINBUILDER -ti toolchain-builder bash"
echo "PASSED"
