FROM docker-registry.apertis.org/apertis/apertis-v2019-package-builder:latest

RUN apt update
RUN apt build-dep -y gcc-8-cross
RUN apt build-dep -y gdb
RUN apt build-dep -y binutils
#RUN apt build-dep -y cross-toolchain-base
RUN apt install -y gcc-8-source
RUN apt install -y linux-source-4.19 glibc-source binutils-source dpkg-cross

#RUN build-all.sh arm64 arm-linux-gnueabihf64
#RUN build-all.sh armhf arm-linux-gnueabihf
RUN bash
